package agenda.test;

import static org.junit.Assert.*;

import java.util.*;

import agenda.controller.ActivityController;
import agenda.controller.UserController;
import agenda.exceptions.InvalidFormatException;
import agenda.model.Activity;
import agenda.model.Contact;
import agenda.model.User;
import agenda.repository.classes.RepositoryActivityFile;
import agenda.repository.classes.RepositoryContactFile;

import agenda.repository.classes.RepositoryUserFile;
import org.junit.Before;
import org.junit.Test;

public class AfisActivityTest {
    private Activity activity;
	RepositoryActivityFile rep = new RepositoryActivityFile();
	ActivityController ctr = new ActivityController(rep);
    private RepositoryUserFile repCon = new RepositoryUserFile();
    private UserController ctrCon = new UserController(repCon);

	public AfisActivityTest() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		rep = new RepositoryActivityFile();
	}

	@Test
	public void testCase1_Valid() throws Exception {
        User con = ctrCon.getByName("name1");
        List<User> lst = new ArrayList<User>();
        lst.add(con);
	    activity = new Activity("marathon", new Date(1525707409000L), new Date(1525707409000L), lst, "Wizz Air", "cluj");
        int n = rep.getAll().size();
        ctr.addActivity(activity);
        Calendar c = Calendar.getInstance();
        c.set(2018, 04, 07);
        Date d = c.getTime();
		List<Activity> lstRez = ctr.activitiesOnDate("name1", d);
		assertEquals(1, lstRez.size());
		ctr.removeActivity(activity);
	}

	@Test
    public void testCase2_Invalid() throws Exception {
        Calendar c = Calendar.getInstance();
        c.set(2018, 04, 29);
        Date d = c.getTime();
        try {
            List<Activity> lstRez = ctr.activitiesOnDate("name1", d);
        }catch (InvalidFormatException ex){
            assertTrue(true);
            assertEquals("Nu exista activitati pentru data introdusa!", ex.getErrorMessage());
        }
    }
}
