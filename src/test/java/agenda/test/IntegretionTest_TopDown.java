package agenda.test;

import agenda.controller.ActivityController;
import agenda.controller.ContactController;
import agenda.controller.UserController;
import agenda.model.Activity;
import agenda.model.Contact;
import agenda.model.User;
import agenda.repository.classes.RepositoryActivityFile;
import agenda.repository.classes.RepositoryContactFile;
import agenda.repository.classes.RepositoryUserFile;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * @author = Nimrod Foldvari
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class IntegretionTest_TopDown {
    private RepositoryActivityFile rep = new RepositoryActivityFile();
    private ActivityController ctr = new ActivityController(rep);
    private RepositoryUserFile repUser = new RepositoryUserFile();
    private UserController ctrUser = new UserController(repUser);
    private RepositoryContactFile repCon = new RepositoryContactFile();
    private ContactController ctrCon = new ContactController(repCon);

    public IntegretionTest_TopDown() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
        repCon = new RepositoryContactFile();
    }

    @Test
    public void testCase1_unitA() throws Exception {

        Contact con = new Contact("Vlad", "addres", "+0764681743");
        if (ctrCon.getByName(con.getName()) != null) {
            ctrCon.remove(con);
        }
        int n = repCon.getAll().size();
        ctrCon.add(con);
        assertEquals(n + 1, rep.getAll().size());
        Contact c = ctrCon.getByName("Vlad");
        assertEquals(c.getTelefon(), "+0764681743");
        ctrCon.remove(con);
    }

    @Test
    public void testCase2_integrateAB() throws Exception {
        boolean part1 = false, part2 = false;

        Contact con = new Contact("Vlad", "addres", "+0764681743");
        int n = repCon.getAll().size();
        ctrCon.add(con);
        if (n + 1 == rep.getAll().size())
            part1 = true;

        User user = ctrUser.getByName("name1");
        List<User> lst = new ArrayList<User>();
        lst.add(user);
        Activity activity = new Activity("marathon", new Date(1591707409000L), new Date(1591707409022L), lst, "Wizz Air", "cluj");
        int nr = rep.getAll().size();
        ctr.addActivity(activity);
        if (nr + 1 == rep.getAll().size())
            part2 = true;

        assertTrue(part1 && part2);

        ctr.removeActivity(activity);
        ctrCon.remove(con);
    }

    @Test
    public void testCase3_integrareABC() throws Exception {
        boolean part1 = false, part2 = false, part3 = false;

        User user = ctrUser.getByName("name1");
        List<User> lst = new ArrayList<User>();
        lst.add(user);
        Activity activity = new Activity("marathon", new Date(1523705606950L), new Date(1523705606955L), lst, "Wizz Air", "cluj");
        int nr = rep.getAll().size();
        ctr.addActivity(activity);
        if (nr + 1 == rep.getCount())
            part2 = true;

        Contact con = new Contact("Popescu", "addres", "+0764681743");
        int n = repCon.getAll().size();
        ctrCon.add(con);
        if (n + 1 == repCon.getAll().size()) ;
        part1 = true;

        Calendar c = Calendar.getInstance();
        c.set(2018, 03, 14);
        Date d = c.getTime();
        List<Activity> lstRez = ctr.activitiesOnDate("name1", d);
        if (lstRez.size() == 1)
            part3 = true;
        assertTrue(part1 && part2 && part3);
        ctr.removeActivity(activity);
        ctrCon.remove(con);
    }

}