//package agenda.test;
//
//import static org.junit.Assert.assertFalse;
//import static org.junit.Assert.assertTrue;
//
//import java.text.DateFormat;
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//
//import agenda.model.Activity;
//
//import agenda.repository.classes.RepositoryActivityFile;
//import org.junit.Before;
//import org.junit.Test;
//
//public class addTest {
//	private Activity act;
//	private RepositoryActivityFile rep;
//
//	@Before
//	public void setUp() throws Exception {
//		rep = new RepositoryActivityFile();
//	}
//
//	@Test
//	public void testCase1() throws Exception {
//		DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
//		try {
//			act = new Activity("name1",
//					df.parse("03/20/2013 12:00"),
//					df.parse("03/20/2013 13:00"),
//					null,
//					"Lunch break", "Cluj");
//			rep.add(act);
//		} catch (ParseException e) {
//			e.printStackTrace();
//		}
//		assertTrue(1 == rep.getCount());
//	}
//
//	@Test
//	public void testCase2()
//	{
//		DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
//		try{
//			for (Activity a : rep.getAll())
//				rep.remove(a);
//
//			act = new Activity("name1",
//					df.parse("03/20/2013 12:00"),
//					df.parse("03/20/2013 13:00"),
//					null,
//					"Lunch break", "Cluj");
//			rep.add(act);
//
//			act = new Activity("name1",
//					df.parse("03/21/2013 12:00"),
//					df.parse("03/21/2013 13:00"),
//					null,
//					"Lunch break", "Cluj");
//			rep.add(act);
//		}
//		catch(Exception e){}
//		int c = rep.getCount();
//		assertTrue( c == 2);
//		for (Activity a : rep.getAll())
//			rep.remove(a);
//	}
//
//	@Test
//	public void testCase3()
//	{
//		DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
//		try{
//			for (Activity a : rep.getAll())
//				rep.remove(a);
//
//			act = new Activity("name1",
//					df.parse("03/20/2013 12:00"),
//					df.parse("03/20/2013 13:00"),
//					null,
//					"Lunch break", "Cluj");
//			rep.add(act);
//
//			act = new Activity("name1",
//					df.parse("03/20/2013 12:30"),
//					df.parse("03/20/2013 13:30"),
//					null,
//					"Lunch break","Cluj");
//			rep.add(act);
//		}
//		catch(Exception e){}
//		assertTrue( 1 == rep.getCount());
//		rep.saveActivities();
//		for (Activity a : rep.getAll())
//			rep.remove(a);
//	}
//
//	@Test
//	public void testCase4()
//	{
//		DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
//		try{
//			for (Activity a : rep.getAll())
//				rep.remove(a);
//
//			act = new Activity("name1",
//					df.parse("03/20/2013 12:00"),
//					df.parse("03/20/2013 13:00"),
//					null,
//					"Lunch break", "Cluj");
//			rep.add(act);
//
//			act = new Activity("name1",
//					df.parse("03/20/2013 13:30"),
//					df.parse("03/20/2013 14:00"),
//					null,
//					"Curs", "Cluj");
//			rep.add(act);
//
//			act = new Activity("name1",
//					df.parse("03/20/2013 13:30"),
//					df.parse("03/20/2013 14:30"),
//					null,
//					"Lunch break", "Cluj");
//			rep.add(act);
//		}
//		catch(Exception e){}
//		assertTrue( 2 == rep.getCount());
//		for (Activity a : rep.getAll())
//			rep.remove(a);
//	}
//
//	@Test
//	public void testCase5()
//	{
//		DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
//		try{
//			for (Activity a : rep.getAll())
//				rep.remove(a);
//
//			act = new Activity("name1",
//					df.parse("03/20/2013 12:00"),
//					df.parse("03/20/2013 13:00"),
//					null,
//					"Lunch break", "Cluj");
//			rep.add(act);
//
//			rep.add(act);
//		}
//		catch(Exception e){}
//		assertTrue( 1 == rep.getCount());
//		for (Activity a : rep.getAll())
//			rep.remove(a);
//	}
//}
