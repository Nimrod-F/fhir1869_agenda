package agenda.test;

import agenda.controller.ActivityController;
import agenda.controller.ContactController;
import agenda.controller.UserController;
import agenda.exceptions.InvalidFormatException;
import agenda.model.Activity;
import agenda.model.Contact;
import agenda.model.User;
import agenda.repository.classes.RepositoryActivityFile;
import agenda.repository.classes.RepositoryContactFile;
import agenda.repository.classes.RepositoryUserFile;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ActivityTest {

    private Activity activity;
    private RepositoryActivityFile rep = new RepositoryActivityFile();
    private ActivityController ctr = new ActivityController(rep);
    private RepositoryUserFile repCon = new RepositoryUserFile();
    private UserController ctrCon = new UserController(repCon);

    public ActivityTest() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
        rep = new RepositoryActivityFile();
    }

    @Test
    public void testCase1() throws Exception {
        User con = ctrCon.getByName("name1");
        List<User> lst = new ArrayList<User>();
        lst.add(con);
        activity = new Activity("meeting", new Date(1521021603925L), new Date(1521023403925L), lst, "sprint 5", "cluj");
        try {
            ctr.addActivity(activity);
            assertTrue(false);
        } catch (InvalidFormatException e) {
            assertTrue(true);
            assertEquals("Activity invalid!", e.getErrorMessage());
        }
    }

    @Test
    public void testCase2() throws Exception {
        User con = ctrCon.getByName("name1");
        List<User> lst = new ArrayList<User>();
        lst.add(con);
        activity = new Activity("marathon", new Date(1521021603925L), new Date(1521021603000L), lst, "sprint 5", "cluj");
        try {
            ctr.addActivity(activity);
            assertTrue(false);
        } catch (InvalidFormatException e) {
            assertTrue(true);
            assertEquals("Activity invalid!", e.getErrorMessage());
        }
    }

    @Test
    public void testCase3() throws Exception {
        User con = ctrCon.getByName("name1");
        List<User> lst = new ArrayList<User>();
        lst.add(con);
        activity = new Activity("marathon", new Date(1363000000000L), new Date(1363777200555L), lst, "sprint 5", "cluj");
        try {
            ctr.addActivity(activity);
            assertTrue(false);
        } catch (InvalidFormatException e) {
            assertTrue(true);
            assertEquals("Activitatea se intersecteaza cu o alta activitate", e.getErrorMessage());
        }

    }

    @Test
    public void testCase4() throws Exception {
        User con = ctrCon.getByName("name1");
        List<User> lst = new ArrayList<User>();
        lst.add(con);
        activity = new Activity("marathon", new Date(1521707409000L), new Date(1521707410000L), lst, "Wizz Air", "cluj");
        int n = rep.getAll().size();
        ctr.addActivity(activity);
        assertEquals(n + 1, rep.getAll().size());
        assertEquals(activity, ctr.activitiesByName("marathon").get(0));
        ctr.removeActivity(activity);
    }
}
