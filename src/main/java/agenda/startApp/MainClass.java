package agenda.startApp;

import agenda.controller.ActivityController;
import agenda.controller.ContactController;
import agenda.controller.UserController;
import agenda.model.Contact;
import agenda.model.User;
import agenda.repository.classes.RepositoryActivityFile;
import agenda.repository.classes.RepositoryContactFile;
import agenda.repository.classes.RepositoryUserFile;
import agenda.repository.interfaces.RepositoryUser;
import agenda.ui.UI;

import java.io.BufferedReader;
import java.io.InputStreamReader;

//functionalitati
//i.	 adaugarea de contacte (nume, adresa, numar de telefon, adresa email);
//ii.	 programarea unor activitati (denumire, descriere, data, locul, ora inceput, durata, contacte).
//iii.	 generarea unui raport cu activitatile pe care le are utilizatorul (nume, user, parola) la o anumita data, ordonate dupa ora de inceput.

public class MainClass {

	public static void main(String[] args) {
		BufferedReader in = null;
		try
		{
			RepositoryContactFile contactRep = new RepositoryContactFile();
			ContactController contractCtr = new ContactController(contactRep);
			RepositoryUserFile userRep = new RepositoryUserFile();
			UserController userCtr = new UserController(userRep);
			RepositoryActivityFile activityRep = new RepositoryActivityFile();
			ActivityController activityCtr = new ActivityController(activityRep);
			UI ui = new UI();
			User user = null;
			in = new BufferedReader(new InputStreamReader(System.in));
			while (user == null) {
				System.out.printf("Enter username: ");
				String u = in.readLine();
				System.out.printf("Enter password: ");
				String p = in.readLine();

				user = userCtr.getByUsername(u);
				if (!user.isPassword(p))
					user = null;
			}

			int chosen = 0;
			while (chosen != 4) {
				ui.printMenu();
				chosen = Integer.parseInt(in.readLine());
				try {
					switch (chosen) {
						case 1:
							ui.adaugContact(contractCtr, in);
							break;
						case 2:
							ui.adaugActivitate(activityCtr, userCtr, in, user);
							break;
						case 3:
							ui.afisActivitate(activityCtr, in, user);
							break;
					}
				} catch (Exception e) {
					System.out.println(e.getMessage());
				}
			}
		} catch(Exception e)
		{
			System.out.println(e.getMessage());
		}
		System.out.println("Program over and out\n");
	}
}
