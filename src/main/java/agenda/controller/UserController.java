package agenda.controller;

import agenda.model.User;
import agenda.repository.classes.RepositoryUserFile;

import java.util.LinkedList;
import java.util.List;

public class UserController {
    private RepositoryUserFile repoU;
    private List<User> users = new LinkedList<User>();
    public UserController(RepositoryUserFile repoU) {
        this.repoU = repoU;
        this.users = repoU.getUsers();
    }

    public User getByPassword(String password) {
        for (User u : users)
            if (u.getPassword().equals(password)) return u;
        return null;
    }


    public User getByUsername(String username) {
        for (User u : users)
            if (u.getUsername().equals(username)) return u;
        return null;
    }


    public User getByName(String name) {
        for (User u : users)
            if (u.getName().equals(name)) return u;
        return null;
    }



}
