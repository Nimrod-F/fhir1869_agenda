package agenda.controller;

import agenda.exceptions.InvalidFormatException;
import agenda.model.Activity;
import agenda.model.Contact;
import agenda.model.validator.ActivityValidator;
import agenda.repository.classes.RepositoryActivityFile;
import agenda.repository.classes.RepositoryContactFile;

import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class ActivityController {
    private RepositoryActivityFile repoA;
    private ContactController ctrC;
    private List<Activity> activities = new LinkedList<Activity>();
    private ActivityValidator validator;
    public ActivityController(RepositoryActivityFile repoA) throws Exception {
        this.repoA = repoA;
        this.activities = repoA.getAll();
        this.validator = new ActivityValidator();
        this.ctrC = new ContactController(new RepositoryContactFile());
    }


    public void addActivity(Activity activity) throws InvalidFormatException {
        boolean val = validator.validate(activity);
        int index = activities.indexOf(activity);
        if (index >= 0||!val) throw new InvalidFormatException("Activity invalid!");
        for (int i = 0; i < activities.size(); i++) {
            if (activity.intersect(activities.get(i))) throw new InvalidFormatException("Activitatea se intersecteaza cu o alta activitate");
        }
        repoA.add(activity);

    }

    public void removeActivity(Activity activity) throws InvalidFormatException {
        int index = activities.indexOf(activity);
        if (index < 0) throw new InvalidFormatException("Activitatea nu exista!");
        repoA.remove(activity);
    }

    public List<Activity> activitiesByName(String name) {
        List<Activity> result = new LinkedList<Activity>();
        for (Activity a : activities)
            if (a.getName().equals(name)) result.add(a);
        return result;
    }

    @SuppressWarnings("deprecation")
    public List<Activity> activitiesOnDate(String name, Date d) throws InvalidFormatException {
        List<Activity> result = new LinkedList<Activity>();
        for (Activity a : activities)
            if(a.getContacts().size()!=0) {
                if (a.getContacts().get(0).getName().equals(name))
                    if ((a.getStart().getYear() == d.getYear() &&
                            a.getStart().getMonth() == d.getMonth() &&
                            a.getStart().getDate() == d.getDate())) result.add(a);
            }
        if(result.size()==0)
            throw new InvalidFormatException("Nu exista activitati pentru data introdusa!");
        return result;
    }
}
