package agenda.controller;

import agenda.exceptions.InvalidFormatException;
import agenda.model.Activity;
import agenda.model.Contact;
import agenda.model.validator.ContactValidator;
import agenda.repository.classes.RepositoryContactFile;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class ContactController {
    private RepositoryContactFile repoC;
    private List<Contact> contacts = new LinkedList<Contact>();
    private ContactValidator validator;
    public ContactController(RepositoryContactFile repoC) throws Exception {
        this.repoC = repoC;
        this.contacts = repoC.getAll();
        this.validator = new ContactValidator();
    }

    public Contact getByName(String string) {
        for (Contact c : contacts)
            if (c.getName().equals(string))
                return c;
        return null;
    }

    public void remove(Contact contact) throws InvalidFormatException {
        int index = contacts.indexOf(contact);
        if (index < 0)
            throw new InvalidFormatException("Contactul nu exista!");
        else {
            repoC.remove(contact);
        }
    }


    public void add(Contact contact) throws Exception {
        boolean exist = false;
        for(Contact c: findAll()){
            if(c==contact)
                exist=true;
        }
        if(exist)
            throw new InvalidFormatException("Contact existent!");
        boolean val = validator.validate(contact);
        if(!val)
            throw new InvalidFormatException("Contact invalid!");
        else
            repoC.add(contact);
    }

    public List<Contact> findAll() throws Exception {
        return this.repoC.getAll();
    }


}
