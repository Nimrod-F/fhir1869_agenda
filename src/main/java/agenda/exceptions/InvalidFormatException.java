package agenda.exceptions;

public class InvalidFormatException extends Exception {
    private static final long serialVersionUID = -6262759468431626763L;

    private String msg;

    public InvalidFormatException(String msg) {
        super(msg);
        this.msg = msg;
    }

    public String getErrorMessage() {
        return msg;
    }

}
