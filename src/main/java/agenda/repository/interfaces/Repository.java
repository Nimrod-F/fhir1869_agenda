package agenda.repository.interfaces;

import java.util.List;

public interface Repository<T> {
    List<T> getAll() throws Exception;
    void add(T t);
    void remove(T t);
    int getCount();
}
