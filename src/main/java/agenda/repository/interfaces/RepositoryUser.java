package agenda.repository.interfaces;

import java.util.List;

import agenda.model.User;

public interface RepositoryUser {

	List<User> getUsers();
	int getCount();
	
}
