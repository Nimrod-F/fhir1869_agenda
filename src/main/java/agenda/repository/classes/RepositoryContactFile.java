package agenda.repository.classes;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;

import agenda.model.Contact;
import agenda.repository.interfaces.Repository;

public class RepositoryContactFile implements Repository<Contact> {

    private static final String filename = "files\\contacts.txt";
    private List<Contact> contacts;

    public RepositoryContactFile() throws Exception {
        contacts = new LinkedList<Contact>();
        readFromFile();
    }

    public Contact fromString(String str, String delim) {
        String[] s = str.split(delim);
        try {
            return new Contact(s[0], s[1], s[2]);
        } catch (Exception e) {
            return null;
        }
    }

    private void readFromFile() throws Exception {
        BufferedReader br = null;
        this.contacts.clear();
        try {
            br = new BufferedReader(new InputStreamReader(new FileInputStream(filename)));
            String line;
            int i = 0;
            while ((line = br.readLine()) != null) {
                Contact c = fromString(line, "#");
                if (c == null) {
                    throw new Exception("Error in file at line " + i, new Throwable("Invalid Contacts"));
                }
                contacts.add(c);
                i++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) br.close();
        }
    }

    @Override
    public List<Contact> getAll() throws Exception {
        readFromFile();
        return contacts;
    }

    @Override
    public void add(Contact contact) {
        contacts.add(contact);
        saveContractsToFile();
    }

    @Override
    public void remove(Contact contact) {
        contacts.remove(contact);
        saveContractsToFile();
    }

    public boolean saveContractsToFile() {
        PrintWriter pw = null;
        try {
            pw = new PrintWriter(new FileOutputStream(filename));
            for (Contact c : contacts)
                pw.println(c.toString());
        } catch (Exception e) {
            return false;
        } finally {
            if (pw != null) pw.close();
        }
        return true;
    }

    @Override
    public int getCount() {
        return this.contacts.size();
    }

}
