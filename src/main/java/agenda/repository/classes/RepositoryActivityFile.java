package agenda.repository.classes;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import agenda.controller.ContactController;
import agenda.controller.UserController;
import agenda.model.Activity;
import agenda.model.Contact;
import agenda.model.User;
import agenda.repository.interfaces.Repository;

public class RepositoryActivityFile implements Repository<Activity> {

    private final String filename = "files\\activities.txt";
    private List<Activity> activities;

    public RepositoryActivityFile() throws Exception {
        activities = new LinkedList<Activity>();
        readFromFile();
    }

    private void readFromFile() throws Exception {
        BufferedReader br = null;
        this.activities.clear();
        try {
            br = new BufferedReader(new InputStreamReader(new FileInputStream(filename)));
            String line;
            int i = 0;
            while ((line = br.readLine()) != null) {
                Activity act = fromString(line);
                if (act == null)
                    throw new Exception("Error in file at line " + i, new Throwable("Invalid Activity"));
                activities.add(act);
                i++;
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            if (br != null) br.close();
        }
    }

    public Activity fromString(String line) {
        try {
            UserController ctr = new UserController(new RepositoryUserFile());
            String[] str = line.split("#");
            String name = str[0];
            Date start = new Date(Long.parseLong(str[1]));
            Date duration = new Date(Long.parseLong(str[2]));
            String description = str[3];
            String location = str[4];
            List<User> conts = new LinkedList<User>();
            for (int i = 5; i < str.length; i++) {
                conts.add(ctr.getByName(str[i]));
            }
            return new Activity(name, start, duration, conts, description, location);
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public List<Activity> getAll() {
        try {
            readFromFile();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return activities;
    }

    @Override
    public void add(Activity activity){
        activities.add(activity);
        saveActivities();
    }

    @Override
    public void remove(Activity activity){
        activities.remove(activity);
        saveActivities();
    }

    public boolean saveActivities() {
        PrintWriter pw = null;
        try {
            pw = new PrintWriter(new FileOutputStream(filename));
            for (Activity a : activities)
                pw.println(a.toString());
        } catch (Exception e) {
            return false;
        } finally {
            if (pw != null) pw.close();
        }
        return true;
    }

    @Override
    public int getCount() {
        return activities.size();
    }


}
