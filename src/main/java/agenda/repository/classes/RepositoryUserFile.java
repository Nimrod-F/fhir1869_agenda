package agenda.repository.classes;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;

import agenda.model.User;
import agenda.repository.interfaces.RepositoryUser;

public class RepositoryUserFile implements RepositoryUser {

    private List<User> users;
    private static final String filename = "files\\users.txt";

    public RepositoryUserFile() throws Exception {
        users = new LinkedList<User>();
        BufferedReader br = null;
        try {
            br = new BufferedReader(new InputStreamReader(new FileInputStream(filename)));
            String line;
            int i = 1;
            while ((line = br.readLine()) != null) {
                User u = fromString(line);
                if (u == null)
                    throw new Exception("Error in file at line " + i, new Throwable("Invalid User"));
                users.add(u);
                i++;
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            if (br != null) br.close();
        }
    }


    public User fromString(String s)
    {
        String[] str = s.split("#");
        try
        {
            return new User(str[0], str[1], str[2]);
        }
        catch (Exception e)
        {
            return null;
        }
    }

    @Override
    public List<User> getUsers() {
        return users;
    }

    @Override
    public int getCount() {
        return users.size();
    }

}
