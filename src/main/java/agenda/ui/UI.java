package agenda.ui;

import agenda.controller.ActivityController;
import agenda.controller.ContactController;
import agenda.controller.UserController;
import agenda.exceptions.InvalidFormatException;
import agenda.model.Activity;
import agenda.model.Contact;
import agenda.model.User;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.*;

public class UI {

    public void afisActivitate(ActivityController activityCtr,
                               BufferedReader in, User user) {
        try {
            System.out.printf("Afisare Activitate: \n");
            System.out.printf("Data(format: mm/dd/yyyy): ");
            String dateS = in.readLine();
            Calendar c = Calendar.getInstance();
            c.set(Integer.parseInt(dateS.split("/")[2]),
                    Integer.parseInt(dateS.split("/")[0]) - 1,
                    Integer.parseInt(dateS.split("/")[1]));
            Date d = c.getTime();

            System.out.println("Activitatile din ziua " + d.toString() + ": ");

            List<Activity> act = activityCtr
                    .activitiesOnDate(user.getName(), d);
            for (Activity a : act) {
                System.out.printf("%s, %s", a.getName(), a.getDescription());
                System.out.println();
            }
        } catch (IOException e) {
            System.out.printf("Eroare de citire: %s\n", e.getMessage());
        } catch (InvalidFormatException e) {
            System.out.println(e.getErrorMessage());
        }
    }

    public void adaugActivitate(ActivityController activityCtr, UserController ctrC, BufferedReader in, User user) {
        try {
            System.out.printf("Adauga Activitate: \n");
            System.out.printf("Nume: ");
            String name = in.readLine();
            System.out.printf("Descriere: ");
            String description = in.readLine();
            System.out.printf("Data de inceput(format: mm/dd/yyyy): ");
            String dateS = in.readLine();
            System.out.printf("Ora de inceput(hh:mm): ");
            String timeS = in.readLine();
            Calendar c = Calendar.getInstance();
            c.set(Integer.parseInt(dateS.split("/")[2]),
                    Integer.parseInt(dateS.split("/")[0]) - 1,
                    Integer.parseInt(dateS.split("/")[1]),
                    Integer.parseInt(timeS.split(":")[0]),
                    Integer.parseInt(timeS.split(":")[1]));
            Date start = c.getTime();

            System.out.printf("Data de sfarsit(format: mm/dd/yyyy): ");
            String dateE = in.readLine();
            System.out.printf("Ora de sfarsit(hh:mm): ");
            String timeE = in.readLine();

            c.set(Integer.parseInt(dateE.split("/")[2]),
                    Integer.parseInt(dateE.split("/")[0]) - 1,
                    Integer.parseInt(dateE.split("/")[1]),
                    Integer.parseInt(timeE.split(":")[0]),
                    Integer.parseInt(timeE.split(":")[1]));
            Date end = c.getTime();
            System.out.printf("Locatia: ");
            String location = in.readLine();
            User contact = ctrC.getByName(user.getName());
            List<User> contacts = new ArrayList<User>();
            contacts.add(contact);
            Activity act = new Activity(name, start, end,
                    contacts, description, location);

            try {
                activityCtr.addActivity(act);
            } catch (InvalidFormatException e1) {
                System.out.println(e1.getErrorMessage());
            }
            System.out.printf("S-a adugat cu succes\n");
        } catch (IOException e) {
            System.out.printf("Eroare de citire: %s\n", e.getMessage());
        }

    }

    public void adaugContact(ContactController ctr,
                             BufferedReader in) throws Exception {

        try {
            System.out.printf("Adauga Contact: \n");
            System.out.printf("Nume: ");
            String name = in.readLine();
            System.out.printf("Adresa: ");
            String adress = in.readLine();
            System.out.printf("Numar de telefon: ");
            String telefon = in.readLine();

            Contact c = new Contact(name, adress, telefon);

            ctr.add(c);

            System.out.printf("S-a adugat cu succes\n");
        } catch (IOException e) {
            System.out.printf("Eroare de citire: %s\n", e.getMessage());
        } catch (InvalidFormatException e) {
            if (e.getCause() != null)
                System.out.printf("Eroare: %s - %s\n", e.getErrorMessage(), e
                        .getCause().getMessage());
            else
                System.out.printf("Eroare: %s\n", e.getMessage());
        }

    }

    public void printMenu() {
        System.out.printf("Alegeti una din obtiuni:\n");
        System.out.printf("1. Adauga contact\n");
        System.out.printf("2. Adauga activitate\n");
        System.out.printf("3. Afisare activitati din data de...\n");
        System.out.printf("4. Exit\n");
        System.out.printf("Alege: ");
    }

}