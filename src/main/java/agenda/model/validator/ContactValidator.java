package agenda.model.validator;

import agenda.model.Contact;

public class ContactValidator {
    public ContactValidator() {
    }

    public boolean validate(Contact contact){
        if (contact.getName().length()<2 || contact.getAddress().length()<5) return false;
        String[] s = contact.getTelefon().split("[\\p{Punct}\\s]+");
        if (contact.getTelefon().charAt(0) != '+') return false;
        if (s[1].charAt(0) != '0' || s[1].length() != 10)return false;
        return true;
    }


}
