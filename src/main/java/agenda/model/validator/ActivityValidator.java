package agenda.model.validator;

import agenda.model.Activity;

public class ActivityValidator {
    public ActivityValidator() {
    }

    public boolean validate(Activity activity){
        if(activity.getStart().compareTo(activity.getDuration()) > 0)
        return false;
        if(activity.getName().length()<2){
            return false;
        }
        if(activity.getDescription().length()<4)
            return false;
        if(activity.getLocation().length()<4)
            return false;
        return true;
    }
}
