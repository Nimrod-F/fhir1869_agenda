package agenda.model;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class Activity {
    private String name;
    private Date start;
    private Date duration;
    private List<User> contacts;
    private String description;
    private String location;

    public Activity(String name, Date start, Date end, List<User> contacts,
                    String description, String location) {
        this.name = name;
        this.description = description;
        if (contacts == null)
            this.contacts = new LinkedList<User>();
        else
            this.contacts = new LinkedList<User>(contacts);

        this.start = new Date();
        this.start.setTime(start.getTime());
        this.duration = new Date();
        this.duration.setTime(end.getTime());
        this.location = location;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getDuration() {
        return duration;
    }

    public void setDuration(Date duration) {
        this.duration = duration;
    }

    public List<User> getContacts() {
        return contacts;
    }

    public void setContacts(List<User> contacts) {
        this.contacts = contacts;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public boolean intersect(Activity act) {
        if ((start.compareTo(act.start) < 0
                && act.duration.compareTo(duration) < 0)|| (act.duration.compareTo(duration)<0 && start.compareTo(act.duration)<0))
            return true;
        return false;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Activity))
            return false;
        Activity act = (Activity) obj;
        if (act.description.equals(description) && start.equals(act.start)
                && duration.equals(act.duration) && name.equals(act.name) && location.equals(act.location) && contacts.equals(act.contacts))
            return true;
        return false;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(name);
        sb.append("#");
        sb.append(start.getTime());
        sb.append("#");
        sb.append(duration.getTime());
        sb.append("#");
        sb.append(description);
        sb.append("#");
        sb.append(location);
        sb.append("#");
        for (User c : contacts) {
            sb.append(c.getName());
            sb.append("#");
        }
        return sb.toString();
    }


}
